import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import Stepper from 'bs-stepper';
import { FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.scss']
})
export class ResultComponent implements OnInit {
  users = [];
  stepper;
  form: FormGroup;
  address = {
    city: '',
    cityAlt: '',
    country: '',
    countryCode: ''
  };

  tipoPersona = '';

  datos = {};
  Check0 = {
    TipoPersona: 0,
    Transporte: 0,
    Televisores: 0,
    Alimentacion: 0,
    Ocupacion: 0
  };

  Check1 = {
    FreqT: 0,
    Alimentos2: 0,
    Alimentos3: 0,
    Viajes: 0,
    Gas: 0,
    Lacteos: 0 
  };

  Check2 = {
    Litros: 0,
    Viajes: 0,
    Energia: 0,
    Carne: 0,
    Puerco: 0,
    Pollo: 0,
    Huevos: 0,
    Leche: 0,
    Hijos: 0
  }

  constructor(
    private apiService: ApiService,
    private formBuilder: FormBuilder
  ) {
    this.form = this.formBuilder.group({
      TipoPersona: 0,
      Transporte: 0,
      Televisores: 0,
      Ocupacion: 0,
      FreqT: 0,
      Alimentos2: 0,
      Alimentos3: 0,
      Viajes: 0,
      Gas: 0,
      Lacteos: 0,
      Litros: 0,
      Energia: 0,
      Carne: 0,
      Puerco: 0,
      Pollo: 0,
      Huevos: 0,
      Leche: 0,
      Hijos: 0,
      Check: -1
    });
  }

  ngOnInit(): void {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        let location: google.maps.LatLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        const geocoder = new google.maps.Geocoder();

        geocoder.geocode({location: location }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK) {
              if (results[1]) {
                  let country = null, countryCode = null, city = null, cityAlt = null;
                  let c, lc, component;

                  for (let r = 0, rl = results.length; r < rl; r += 1) {
                      let result = results[r];
      
                      if (!city && result.types[0] === 'locality') {
                          for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
                              component = result.address_components[c];
      
                              if (component.types[0] === 'locality') {
                                  city = component.long_name;
                                  break;
                              }
                          }
                      } else if (!city && !cityAlt && result.types[0] === 'administrative_area_level_1') {
                          for (c = 0, lc = result.address_components.length; c < lc; c += 1) {
                              component = result.address_components[c];
      
                              if (component.types[0] === 'administrative_area_level_1') {
                                  cityAlt = component.long_name;
                                  break;
                              }
                          }
                      } else if (!country && result.types[0] === 'country') {
                          country = result.address_components[0].long_name;
                          countryCode = result.address_components[0].short_name;
                      }
      
                      if (city && country) {
                          break;
                      }
                  }
      
                  this.address.city = city;
                  this.address.cityAlt = cityAlt;
                  this.address.country = country;
                  this.address.countryCode = countryCode;
              }
          }
        }); 

      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

  ngAfterViewInit() {
    this.stepper = new Stepper(document.querySelector('.bs-stepper'));
  }

  next(data) {
    this.form.controls['Check'].setValue(data);
    this.stepper.next();
    this.submit();
  }

  previous() {
    this.stepper.previous();
  }

  lastCheck() {
    this.form.controls['Check'].setValue(2);
  }

  submit() {
    console.log(this.form.value);

    this.apiService.postData(this.form.value).subscribe(data => {});
  }
}
