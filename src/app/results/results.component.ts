import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  result1 = {
    img: '',
    name: '',
    description: '',
    address: '',
    email: '',
    phone: '',
    certified: false
  };

  results = [
    {
      img: 'assets/imgs/card-image.png',
      name: 'Company 1',
      description: 'Short description of company',
      address: 'Address 1234, State',
      email: 'email@company.com',
      phone: '123 456 7890',
      certified: false
    },
    {
      img: 'assets/imgs/card-image.png',
      name: 'Company 2',
      description: 'Short description of company',
      address: 'Address 1234, State',
      email: 'email@company.com',
      phone: '123 456 7890',
      certified: true
    },
    {
      img: 'assets/imgs/card-image.png',
      name: 'Company 3',
      description: 'Short description of company',
      address: 'Address 1234, State',
      email: 'email@company.com',
      phone: '123 456 7890',
      certified: false
    },
    {
      img: 'assets/imgs/card-image.png',
      name: 'Company 4',
      description: 'Short description of company',
      address: 'Address 1234, State',
      email: 'email@company.com',
      phone: '123 456 7890',
      certified: false
    },
    {
      img: 'assets/imgs/card-image.png',
      name: 'Company 5',
      description: 'Short description of company',
      address: 'Address 1234, State',
      email: 'email@company.com',
      phone: '123 456 7890',
      certified: true
    },
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
